<?php
include APPPATH.'libraries/REST_Controller.php';
class Todo extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Todo_model','todo');// load model and alias 
                                                // its name to todo
        
    }
  	public function getAllTodos_post(){
        // check access key
        $accessKey = $this->post('accessKey');
        $this->_checkAccessKey($accessKey);

        
        // Steps to build getAllTodos 
        // 1. Call to model
        $data = $this->todo->getAllTodos(); 

        // 2. response data
            // call to helper to package data
        $response = messsage_success($data); 
            // response data to client in JSON format
    	$this->response($response); 
  	}

  	public function getTodoById_post(){
  		//echo 'get todo by id';
        // steps to build this service
        // 1. get parameter value of todo id and check require parameter
        $todoId = $this->post('todoId');
        $todoId = (int)$todoId;

        if(is_null($todoId) || empty($todoId)){
            $this->response(messsage_error('missing todoId parameter'));
        }

        // 2. call to model with that id
        $data = $this->todo->getTodoById($todoId);
      
        // 3. response to client 
        if(empty($data)) $this->response(messsage_error('todoId='.$todoId.' is not exist'));
        else $data = $data[0];  // access to object the first element in array set

        $response = messsage_success($data);
        $this->response($response);
  	}

  	public function addTodo_post(){
        //Steps to add a new todo
        // 1. get data from client
        $data = array(
            'title' => $this->post('title'),
            'description' => $this->post('description'),
            'user_id' => $this->post('user_id'),
        ); 

        $data['create_date'] = date(FORMAT_DATE);
        $data['update_date'] = date(FORMAT_DATE);
      
            // check require parameter condition
        if(is_null($data['title']) || empty($data['title'])){
            $this->response(messsage_error('title can not be null or empty'));
        }
        if(is_null($data['user_id']) || empty($data['user_id'])){
            $this->response(messsage_error('a todo must have an owner! Missing parameter user_id'));
        }
        if(is_null($data['description']) || empty($data['description'])){
            $this->response(messsage_error('description can not be null or empty'));
        }

        // 2. call to model
        $query = $this->todo->addTodo($data);
     
        // 3. resonse
        //$response = $query->result();
        $this->response(messsage_success($query));
  	}

  	public function updateTodo_post(){
        //Step to udate a todo
        
        //1.get parameter value from client, todoId
        $todoId = $this->post('todoId');
        $title = $this->post('title');
        $description = $this->post('description');

        $newData = array();

            // check condition 
        if(is_null($todoId) || empty($todoId)){
            $this->response(messsage_error('missing todoId parameter. To update a todo, you must give a todo id'));
        }

            // check for data to update
        if(!is_null($title) ||!empty($title)){
            $newData['title'] = $title;
        }
        if(!is_null($description) || !empty($description)){
            $newData['description'] = $description;
        }
        $newData['update_date'] = date(FORMAT_DATE);

        //2.call to model
        $data = $this->todo->updateTodo($todoId, $newData);

        //3.response data
        $data = $data[0];
        $this->response(messsage_success($data)); 

  	}

  	public function deleteTodo_post()
    {
          // Steps to delet todo
      // 1. get parameter value , todo id
      $todoId = $this->post('todoId');
      $userId = $this->post('userId');

      // check condition 
      if(is_null($todoId) || empty($todoId)){
        $this->response(messsage_error('missing todoId parameter. To delete a todo, you must give a todo id'));
      }

      if(is_null($userId) || empty($userId)){
        $this->response(messsage_error('missing userId parameter. To delete a todo, you must make sure that you are the owner of this todo'));
      }

      // 2. call to model
      $query = $this->todo->deleteTodo($todoId, $userId);
      
      // 3. response data
      $this->response(messsage_success('success delete todo id='.$todoId));
  	}


    //*********** Private function
    private function _checkAccessKey($accessKey)
    {
        // check require parameter 
        if(is_null($accessKey) || empty($accessKey)){
           $this->response(messsage_error('Oop Error! no accessKey given!'));
        }

        // check access key value
        if($accessKey != ACCESS_KEY){
            $this->response(messsage_error('Oop Error! accessKey is invalid'));
        }
    }
}







// $paramId = $this->post('id');

// $data = array(
//  "name" => "Test",
//  "age" => 2,
//  "id" => $paramId
//  // "data" => array(
//  //  "code" => 1,
//  //  "message" => "demo message"
//  // )
// );


?>