<script type="text/javascript">
	var HEADERS ={
		'Authorization': 'Basic '+btoa('admin:12345') //js use btoa('user:password')  or php use = base64encode() YWRtaW46MTIzNA==
	};
	var ACCESS_KEY = "myGIC20!6";
	var PROJECT_URL = window.location.href;
	console.log(PROJECT_URL);

	function showAllTodos (data) {
		console.log(data);
		console.log(data[0].title);
		var mydata = '';
	        			
		for (var i = 0; i < data.length; i++) {
			mydata += '<tr>'+
						'<td>'+data[i].title+'</td>'+
						'<td>'+data[i].description+'</td>'+
						'<td>'+data[i].ownername+'</td>'+
						'<td style="vertical-align: middle;">'+
							'<button class="btn btn-primary btn-xs" aria-label="Left Align" onclick="getTodoById('+data[i].id+')" id="btn-editTodo">'+
							  '<span class="glyphicon  glyphicon-eye-open" ></span> View'+
							'</button>'+
							'<button class="btn btn-primary btn-xs" aria-label="Left Align" onclick="editTodo('+data[i].id+')">'+
			  					'<span class="glyphicon  glyphicon-pencil" ></span> Edit'+
							'</button>'+
							'<button class="btn btn-danger btn-xs" aria-label="Left Align" onclick="editTodo('+data[i].id+')">'+ 
			  					'<span class="glyphicon  glyphicon-remove"></span> Delete'+
							'</button>'+
						'</td>'+
					  '<tr>'			  
		};
		//console.log(mydata);debugger;
		$(mydata).appendTo('#tbl-todo');
	}

	function getTodoById (todoId) {
		console.log('getTodoById : '+todoId);
		var obj = {
			accessKey: ACCESS_KEY,
			todoId: todoId
		};
		$.ajax( {
			url: PROJECT_URL+"Todo/getTOdoById", // request webservice url
			method: 'POST',		// request method : post,get
			data: obj,	// parameter via post
			headers: HEADERS,	// setting header
			success: function(response){  // data response 
				console.log(response);
				if(response['code'] == 1){ // success
					console.log(response['data']);

					$('#id').text('ID: '+response['data'].id);
					$('#title').text('Title: '+response['data'].title);
					$('#description').text('Description: '+response['data'].description);
					$('#ownername').text('Owner: '+response['data'].ownername);
					$('#ownerId').text('Owner Id: '+response['data'].owner_id);
					$('#myModal').modal("show");

				}else{ // error

				}
			}
		});
	}

	function getAllTodos () {
		alert('show all todos');
		//console.log(btoa('1234'));
		var obj = {
			accessKey: ACCESS_KEY
		};
		$.ajax( {
			url: PROJECT_URL+"Todo/getAllTodos", // request webservice url
			method: 'POST',		// request method : post,get
			data: obj,	// parameter via post
			headers: HEADERS,	// setting header
			success: function(response){  // data response 
				console.log(response);
				if(response['code'] == 1){ // success
					showAllTodos(response['data']);
				}else{ // error

				}
			}
		});
	}


	function addTodo () {
		alert('add todo');
	}

	function editTodo (todoId) {
		alert('edit todo: '+todoId);
	}

	function deleteTodo (todoId) {
		alert('delete todo: '+todoId);
	}

</script>