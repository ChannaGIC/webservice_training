<?php include 'header.php';?>

	<button class="btn btn-success" onclick="getAllTodos()" id="btn-getAllTodos">List all todos</button>
	<button class="btn btn-warning" onclick="addTodo()" id="btn-addTodo"><span class="glyphicon glyphicon-plus"></span>Add</button>

	<table class="table table-hover" id="tbl-todo">
	    <thead>
	      <tr>
	        <th>Title</th>
	        <th>Description</th>
	        <th>Owner</th>
	        <th>Tools</th>
	      </tr>
	    </thead>
	    <tbody>
	      <tr>
	        <td>John</td>
	        <td>Doe</td>
	        <td>john@example.com</td>
	        <td style="vertical-align: middle;">
	        	<button class="btn btn-primary btn-xs" aria-label="Left Align" onclick="getTodoById(1)">
	        	  <span class="glyphicon  glyphicon-eye-open" ></span> View
	        	</button>
	        	<button class="btn btn-primary btn-xs" aria-label="Left Align" onclick="editTodo(1)">
	        	  <span class="glyphicon  glyphicon-pencil" ></span> Edit
	        	</button>
	        	<button class="btn btn-danger btn-xs" aria-label="Left Align" onclick="deleteTodo(1)"> 
	        	  <span class="glyphicon  glyphicon-remove" ></span> Delete
	        	</button>
	        </td>
	      </tr>
	      <tr>
	        <td>Mary</td>
	        <td>Moe</td>
	        <td>mary@example.com</td>
	        <td style="vertical-align: middle;">
	        	<button class="btn btn-primary btn-xs" aria-label="Left Align" onclick="editTodo(1)" id="btn-editTodo">
	        	  <span class="glyphicon  glyphicon-eye-open" ></span> View
	        	</button>
	        	<button class="btn btn-primary btn-xs" aria-label="Left Align" >
	        	  <span class="glyphicon  glyphicon-pencil" ></span> Edit
	        	</button>
	        	<button class="btn btn-danger btn-xs" aria-label="Left Align" > 
	        	  <span class="glyphicon  glyphicon-remove" ></span> Delete
	        	</button>
	        </td>
	      </tr>
	    </tbody>
	  </table>


	
<?php include 'popup_modal.php';?>
<?php include 'event_handler.php';?>
<?php include 'footer.php';?>

