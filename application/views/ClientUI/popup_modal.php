<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Information of a TODO</h4>
      </div>
      <div class="modal-body">
            <table>
                <tr>
                    <td id="id"></td>
                </tr>
                <tr>
                    <td id="title"></td>
                </tr>
                <tr>
                    <td id="description"></td>
                </tr>
                <tr>
                    <td id="ownername"></td>
                </tr>
                <tr>
                    <td id="ownerId"></td>
                </tr>

            </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>