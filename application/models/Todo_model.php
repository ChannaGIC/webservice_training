<?php 
	class Todo_model extends CI_Model {
		public function __construct(){
            parent::__construct();
        }

        public function getAllTodos(){
        	 //$query = $this->db->get('tbl_todo');
            
                // one line query => easy reading
            // $this->db->select('*');
            // $this->db->from('tbl_todo');
            // $this->db->join('tbl_user', 'tbl_todo.user_id = tbl_user.id');
            // $query = $this->db->get();

                // multiple line calling
            // $query = $this->db->select('*')
            //         ->from('tbl_todo')
            //         ->join('tbl_user', 'tbl_todo.user_id = tbl_user.id')
            //         ->get();
            
                // using sql raw query       
            $query = $this->db->query('SELECT tbl_todo.id,
                                            tbl_todo.title,
                                            tbl_todo.description, 
                                            tbl_user.name AS ownername,  
                                            tbl_user.id AS owner_id 
                                        FROM tbl_user, tbl_todo 
                                        WHERE tbl_todo.user_id=tbl_user.id');
            $data = $query->result();
        	return $data;
        }

        public function getTodoById($todoId){
            $query = $this->db->query('SELECT tbl_todo.id,
                                            tbl_todo.title,
                                            tbl_todo.description, 
                                            tbl_user.name AS ownername,  
                                            tbl_user.id AS owner_id 
                                        FROM tbl_user, tbl_todo 
                                        WHERE tbl_todo.user_id=tbl_user.id
                                            AND tbl_todo.id='.$todoId
                                        );
            $data = $query->result();
            return $data;
        }

        public function addTodo($data)
        {
            $this->db->insert('tbl_todo', $data);
            return $data;
        }

        public function updateTodo($id, $dataUpdate)
        {
            // update data with the given id
            $this->db->where('id', $id);
            $this->db->update('tbl_todo', $dataUpdate);

            // get data just updated
            $data = $this->getTodoById($id);
            return $data;
        } 

        public function deleteTodo($todoId, $userId)
        {
            // delte data with the given id
            $this->db->delete('tbl_todo', array('id' => $todoId, 'user_id' => $userId));
            return $todoId;
        }	




	}
 ?>